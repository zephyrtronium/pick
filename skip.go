/*
Copyright 2025 Branden J Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package pick

import (
	"iter"
	"math"
	"math/rand/v2"
)

// Skip computes numbers of elements to skip between samples to draw a single
// term uniformly from arbitrarily sized sequences.
//
// To draw a sample, create a new Skip, accept the first term of the sequence,
// skip N(rand(), rand()) terms, accept the next, and repeat N followed by an
// accept until the sequence is exhausted.
//
// Conceptually, drawing a sample from an arbitrary sequence can be performed
// by assigning each term a random weight and selecting the term that receives
// the largest.
// Skip transforms this procedure into modeling the number of
// random numbers it would take to find the next larger weight.
// Using this approach allows uniform sampling with O(log n) random numbers
// instead of O(n).
//
// Skip is a low-level primitive that enables complicated sampling procedures,
// especially where drawing a sample is expensive.
// E.g., it can allow iterating over rows in a database without having to
// materialize each row.
// For simple sequences, [Seq] and [Seq2] allow easy sampling.
//
// Skip is appropriate only for drawing one single element from a sequence.
// Sampling more than one element requires a more expensive procedure which is
// not implemented here. (The general technique is called reservoir sampling.)
type Skip struct {
	u float64
}

// N computes the next skip length given two uniformly distributed random uint64s.
// The typical call will look like:
//
//	s.N(rand.Uint64(), rand.Uint64())
func (s *Skip) N(a, b uint64) uint64 {
	// We implement "Algorithm L" as described at
	// https://en.wikipedia.org/w/index.php?title=Reservoir_sampling&oldid=1237583224#Optimal:_Algorithm_L
	// with an adjustment: Algorithm L uses a term that decreases toward zero,
	// but we can instead make it increase toward 1 so that the zero value of a
	// Skip becomes useful.
	// Technically this can cause us to lose precision such that we are
	// no longer uniform over sequences with more than 2^53 terms.
	// I think that's an acceptable cost, as just iterating an empty loop that
	// many times would take unacceptably long.
	x := float64(a>>11) * 0x1p-53 // convert to [0, 1)
	y := float64(b>>11) * 0x1p-53
	// Again, Algorithm L updates the parameter after computing the skip length,
	// but we can do it beforehand so that an input value of 0 still does
	// the right thing.
	s.u = 1 - x*(1-s.u)
	return uint64(math.Log(y) / math.Log(s.u))
}

// Seq picks a single randomly chosen element from an arbitrary sequence.
// If s yields no terms, the result is the zero value of T.
func Seq[T any](s iter.Seq[T]) (u T) {
	var skip Skip
	var n uint64
	for x := range s {
		if n > 0 {
			n--
			continue
		}
		u = x
		n = skip.N(rand.Uint64(), rand.Uint64())
	}
	return u
}

// Seq2 picks a single randomly chosen pair from an arbitrary sequence of pairs.
// If s yields no terms, the results are the zero values of U and V.
func Seq2[U, V any](s iter.Seq2[U, V]) (u U, v V) {
	var skip Skip
	var n uint64
	for x, y := range s {
		if n > 0 {
			n--
			continue
		}
		u, v = x, y
		n = skip.N(rand.Uint64(), rand.Uint64())
	}
	return u, v
}
