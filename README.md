# pick [![Go Reference](https://pkg.go.dev/badge/gitlab.com/zephyrtronium/pick.svg)](https://pkg.go.dev/gitlab.com/zephyrtronium/pick)

Package pick provides utilities for picking things at random.
It contains two varieties of picking things: Dist for reusable random bags and Skip for one-shot random sequence sampling.


## Dist

Informally, pick.Dist lets you construct a bag with 10× as many red balls as green balls and 5× as many green balls as chartreuse balls, then pull 1.4 zillion balls out of the bag and actually find about 10× as many red balls as green balls and about 5× as many green balls as chartreuse balls.

Formally, a Dist is a categorical distribution constructed according to relative weight.
Particular care is taken to ensure the output sampled distribution matches the input distribution as closely as possible, up to precision.

Dist is designed for distributions that are created once and then drawn from many times.
Creating a Dist is an expensive operation.
If you were using Dist for a game, think "once per frame" or "once per entity," rather than "once per entity update."
Selecting variates takes time logarithmic with the number of categories but independent of weights.

Randomness is left to the caller.
You need only provide a uniform uint32, regardless of how you want to get it.

```go
rng := rand.New(rand.NewPCG(2, 2))

d := pick.New([]pick.Case[string]{
	{E: "bocchi", W: 10},
	{E: "nijika", W: 6},
	{E: "ryou", W: 8},
	{E: "kita", W: 8},
})
for i := 0; i < 5; i++ {
	fmt.Println(d.Pick(rng.Uint32()))
}
```


## Skip

Skip allows efficiently pulling a single random element out of a sequence of any size in only the time it takes to iterate through it.

We can conceptually understand the sampling process as assigning a random number to each element of the sequence and then choosing the element which gets the highest number.
Skip transforms this into modeling the distance between successively larger random numbers, so we only need O(log n) of them instead of O(n).

Skip itself is a low-level primitive for high performance or exotic scenarios.
E.g., it can allow for selecting a random result from a database query without having to materialize each row, or dynamically choosing to add more elements to the sequence.
The convenience functions pick.Seq and pick.Seq2 allow pulling a random result from simple sequences.


## License

Copyright 2024 Branden J Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this work except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
