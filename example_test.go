/*
Copyright 2024 Branden J Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package pick_test

import (
	"fmt"
	"math/rand/v2"

	"gitlab.com/zephyrtronium/pick"
)

func Example() {
	rng := rand.New(rand.NewPCG(2, 2))

	d := pick.New([]pick.Case[string]{
		{E: "bocchi", W: 10},
		{E: "nijika", W: 6},
		{E: "ryo", W: 8},
		{E: "kita", W: 8},
	})
	for i := 0; i < 5; i++ {
		fmt.Println(d.Pick(rng.Uint32()))
	}

	// Output:
	// bocchi
	// kita
	// ryo
	// nijika
	// bocchi
}

func ExampleFromMap() {
	type Emote struct {
		Text   string
		Weight int
	}
	// Define a list of emotes we want to choose at random.
	// Note it contains duplicates.
	emotes := []Emote{
		{"☆*: .｡. o(≧▽≦)o .｡.:*☆", 5},
		{"☆*: .｡. o(≧▽≦)o .｡.:*☆", 1},
		{"(*/ω＼*)", 1},
		{"😂👌", 1},
		{"(*/ω＼*)", 1},
		{"☆*: .｡. o(≧▽≦)o .｡.:*☆", 1},
	}
	// If we convert to cases directly, we get six categories.
	cases := make([]pick.Case[string], len(emotes))
	for i, e := range emotes {
		cases[i] = pick.Case[string]{E: e.Text, W: e.Weight}
	}
	fmt.Println(pick.New(cases).Categories() == 6)
	// If we convert to a map first and use FromMap, the duplicates are
	// consolidated.
	m := make(map[string]int, len(emotes))
	for _, e := range emotes {
		m[e.Text] += e.Weight
	}
	fmt.Println(pick.New(pick.FromMap(m)).Categories() == 3)

	// Output:
	// true
	// true
}
