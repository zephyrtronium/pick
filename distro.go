/*
Copyright 2024 Branden J Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package pick provides utilities for picking things at random.
package pick

// Dist is a categorical distribution for efficient random selection.
//
// Informally, a Dist lets you construct a bag with 10× as many red
// balls as green balls and 5× as many green balls as chartreuse balls, then
// pull 1.4 zillion balls out of the bag and actually find 10× as many red
// balls as green balls and 5× as many green balls as chartreuse balls.
//
// Dist is designed for distributions that are created once and then drawn
// from many times. Creating a distribution is an expensive operation.
// Selecting variates takes time logarithmic with the number of categories but
// independent of weights.
//
// The zero value of a Dist contains infinitely many of the zero value of T.
type Dist[T any] struct {
	// cdf is the distribution. The last element is ^uint32(0).
	cdf []uint32
	// elems is the categories.
	elems []T
}

// New creates a categorical distribution from a list of cases.
// Cases with non-positive weights are ignored. Otherwise, listing the same
// category in multiple cases is approximately equivalent to listing the
// category once with the sum of their positive weights.
//
// The time to draw from the distribution is logarithmic in the number of cases.
// If the cases are not fixed but are comparable, it may be more efficient to
// use [FromMap] to consolidate them.
func New[T any](c []Case[T]) *Dist[T] {
	d := Dist[T]{
		elems: make([]T, 0, len(c)),
	}
	var sum float64
	cum := make([]float64, 0, len(c))
	for _, c := range c {
		if c.W <= 0 {
			continue
		}
		sum += float64(c.W)
		cum = append(cum, sum)
		d.elems = append(d.elems, c.E)
	}
	d.cdf = make([]uint32, len(cum))
	for i, v := range cum {
		d.cdf[i] = uint32(v / sum * 0xffffffff)
	}
	if len(d.cdf) > 0 {
		// Guarantee cdf.
		d.cdf[len(d.cdf)-1] = 0xffffffff
	}
	return &d
}

// Pick selects the category corresponding to the given uniform variate. If
// the distribution is empty, the result is the zero value of T.
func (d *Dist[T]) Pick(v uint32) T {
	// The cdf is naturally monotonic, so we can trivially proceed by binary
	// search, switching to linear search at a width threshold.
	// The naturally justifiable threshold is 16 because that works out to the
	// size of a cache line (on amd64). Choosing a threshold rigorously would
	// take more resources (i.e. computers) than I have convenient access to.
	const threshold = 16
	cdf := d.cdf
	l, r := 0, len(cdf)-1
	for r-l >= threshold {
		k := l + (r-l)/2
		if v <= cdf[k] {
			r = k
		} else {
			l = k + 1
		}
	}
	for l <= r {
		if v <= cdf[l] {
			return d.elems[l]
		}
		l++
	}
	var zero T
	return zero
}

// Categories returns the actual number of categories in the distribution.
func (d *Dist[T]) Categories() int {
	return len(d.cdf)
}

// Case is a category for a distribution.
type Case[T any] struct {
	// E is the category.
	E T
	// W is its relative weight.
	W int
}

// FromMap converts a map of categories to their weights to a slice of cases.
// Elements with non-positive weights are discarded.
func FromMap[T comparable](m map[T]int) []Case[T] {
	r := make([]Case[T], 0, len(m))
	for k, v := range m {
		if v > 0 {
			r = append(r, Case[T]{k, v})
		}
	}
	return r
}
