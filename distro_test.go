/*
Copyright 2024 Branden J Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package pick_test

import (
	"fmt"
	"math/rand/v2"
	"slices"
	"strings"
	"testing"

	"gitlab.com/zephyrtronium/pick"
)

var hugeCaseList = func() []pick.Case[string] {
	r := make([]pick.Case[string], 64*64*64)
	for i := range r {
		r[i] = pick.Case[string]{E: fmt.Sprint(i), W: 1}
	}
	return r
}()

func TestDist(t *testing.T) {
	type choice struct {
		v uint32
		w string
	}
	cases := []struct {
		name  string
		elems []pick.Case[string]
		pick  []choice
	}{
		{
			name:  "empty",
			elems: nil,
			pick: []choice{
				{0, ""},
				{1, ""},
				{0x7fffffff, ""},
				{0x80000000, ""},
				{0xffffffff, ""},
			},
		},
		{
			name: "single",
			elems: []pick.Case[string]{
				{E: "bocchi", W: 1},
			},
			pick: []choice{
				{0, "bocchi"},
				{1, "bocchi"},
				{0x7fffffff, "bocchi"},
				{0x80000000, "bocchi"},
				{0xffffffff, "bocchi"},
			},
		},
		{
			name: "single-weighted",
			elems: []pick.Case[string]{
				{E: "bocchi", W: 100},
			},
			pick: []choice{
				{0, "bocchi"},
				{1, "bocchi"},
				{0x7fffffff, "bocchi"},
				{0x80000000, "bocchi"},
				{0xffffffff, "bocchi"},
			},
		},
		{
			name: "double",
			elems: []pick.Case[string]{
				{E: "bocchi", W: 1},
				{E: "ryou", W: 1},
			},
			pick: []choice{
				{0, "bocchi"},
				{1, "bocchi"},
				{0x7fffffff, "bocchi"},
				{0x80000000, "ryou"},
				{0xffffffff, "ryou"},
			},
		},
		{
			name: "double-weighted",
			elems: []pick.Case[string]{
				{E: "bocchi", W: 100},
				{E: "ryou", W: 100},
			},
			pick: []choice{
				{0, "bocchi"},
				{1, "bocchi"},
				{0x7fffffff, "bocchi"},
				{0x80000000, "ryou"},
				{0xffffffff, "ryou"},
			},
		},
		{
			name:  "huge",
			elems: hugeCaseList,
			pick: []choice{
				{0, "0"},
				{1, "0"},
				{0x7fffffff, "131071"},
				{0x80000000, "131072"},
				{0xffffffff, "262143"},
			},
		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			d := pick.New(c.elems)
			for _, p := range c.pick {
				r := d.Pick(p.v)
				if r != p.w {
					t.Errorf("wrong pick with variate %#x: want %v, got %v", p.v, p.w, r)
				}
			}
		})
	}
}

func TestFromMap(t *testing.T) {
	cases := []struct {
		name  string
		elems map[string]int
		want  []pick.Case[string]
	}{
		{
			name:  "empty",
			elems: nil,
			want:  nil,
		},
		{
			name: "single",
			elems: map[string]int{
				"bocchi": 1,
			},
			want: []pick.Case[string]{
				{E: "bocchi", W: 1},
			},
		},
		{
			name: "double",
			elems: map[string]int{
				"bocchi": 1,
				"ryou":   2,
			},
			want: []pick.Case[string]{
				{E: "bocchi", W: 1},
				{E: "ryou", W: 2},
			},
		},
		{
			name: "zero",
			elems: map[string]int{
				"bocchi": 1,
				"ryou":   0,
			},
			want: []pick.Case[string]{
				{E: "bocchi", W: 1},
			},
		},
		{
			name: "negative",
			elems: map[string]int{
				"bocchi": 1,
				"ryou":   -1,
			},
			want: []pick.Case[string]{
				{E: "bocchi", W: 1},
			},
		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			r := pick.FromMap(c.elems)
			slices.SortFunc(r, func(a, b pick.Case[string]) int { return strings.Compare(a.E, b.E) })
			if !slices.Equal(r, c.want) {
				t.Errorf("wrong cases:\nwant %#v\ngot  %#v", c.want, r)
			}
		})
	}
}

func TestCategories(t *testing.T) {
	cases := []struct {
		name  string
		elems []pick.Case[string]
		want  int
	}{
		{
			name:  "empty",
			elems: nil,
			want:  0,
		},
		{
			name: "single",
			elems: []pick.Case[string]{
				{E: "bocchi", W: 1},
			},
			want: 1,
		},
		{
			name: "double",
			elems: []pick.Case[string]{
				{E: "bocchi", W: 1},
				{E: "ryou", W: 1},
			},
			want: 2,
		},
		{
			name: "zero",
			elems: []pick.Case[string]{
				{E: "bocchi", W: 0},
				{E: "ryou", W: 0},
			},
			want: 0,
		},
	}
	for _, c := range cases {
		c := c
		t.Run(c.name, func(t *testing.T) {
			d := pick.New(c.elems)
			if d.Categories() != c.want {
				t.Errorf("wrong number of categories: want %d, got %d", c.want, d.Categories())
			}
		})
	}
}

func BenchmarkDist(b *testing.B) {
	sizes := []int{1, 8, 64, 64 * 64, 64 * 64 * 64}
	for _, s := range sizes {
		c := make([]pick.Case[byte], s)
		for i := range c {
			c[i].W = i + 1
		}
		d := pick.New(c)
		rng := rand.New(rand.NewPCG(1, 1))
		b.Run(fmt.Sprintf("%d/first", s), func(b *testing.B) {
			var x byte
			for i := 0; i < b.N; i++ {
				x += d.Pick(0)
			}
			Sink = x
		})
		b.Run(fmt.Sprintf("%d/last", s), func(b *testing.B) {
			var x byte
			for i := 0; i < b.N; i++ {
				x += d.Pick(^uint32(0))
			}
			Sink = x
		})
		b.Run(fmt.Sprintf("%d/random", s), func(b *testing.B) {
			var x byte
			for i := 0; i < b.N; i++ {
				x += d.Pick(rng.Uint32())
			}
			Sink = x
		})
	}
}

var Sink byte
