package pick_test

import (
	"fmt"
	"iter"
	"math/rand/v2"
	"slices"
	"testing"

	"gitlab.com/zephyrtronium/pick"
)

// chisq computes the chi square test statistic for uniformity of an array of
// 11 frequencies, as well as whether the statistic is sufficient to reject
// uniformity at the p=0.001 level.
func chisq(counts [11]int, N int) (float64, bool) {
	var x2 float64
	for _, o := range counts {
		x2 += float64(o * o)
	}
	x2 /= float64(N) / float64(len(counts))
	x2 -= float64(N)
	return x2, x2 > 29.59
}

func TestSkip(t *testing.T) {
	var counts [11]int // small array so i can use rejection criterion from a table
	const N = 50000
	for range N {
		var s pick.Skip
		var k int
		for {
			d := int(s.N(rand.Uint64(), rand.Uint64()))
			// Increment the skip size to represent accepting a term.
			d++
			if k+d >= len(counts) {
				break
			}
			k += d
		}
		counts[k]++
	}
	x2, reject := chisq(counts, N)
	if reject {
		t.Errorf("uniformity rejected at p=0.001 level with statistic %.3f\n%d", x2, counts)
	}
}

func enum(n int) iter.Seq[int] {
	return func(yield func(int) bool) {
		for i := range n {
			if !yield(i) {
				break
			}
		}
	}
}

func TestSeq(t *testing.T) {
	var counts [11]int
	const N = 50000
	for range N {
		k := pick.Seq(enum(len(counts)))
		counts[k]++
	}
	x2, reject := chisq(counts, N)
	if reject {
		t.Errorf("uniformity rejected at p=0.001 level with statistic %.3f\n%d", x2, counts)
	}
}

func TestSeq2(t *testing.T) {
	var counts [11]int
	const N = 50000
	for range N {
		k, _ := pick.Seq2(slices.All(counts[:]))
		counts[k]++
	}
	x2, reject := chisq(counts, N)
	if reject {
		t.Errorf("uniformity rejected at p=0.001 level with statistic %.3f\n%d", x2, counts)
	}
}

func BenchmarkSkip(b *testing.B) {
	for range b.N {
		var s pick.Skip
		s.N(rand.Uint64(), rand.Uint64())
	}
}

func ExampleSkip() {
	// This example shows a way to use Skip to choose a uniform random element
	// from a map. Some notes on this:
	// - Ranging over the map and choosing the first term gets a randomly
	//   selected element (in practice), but not a uniformly chosen one. Some
	//   items will never be picked based on the construction of the map.
	// - An easier way to do this is to use pick.Seq2(maps.All(options)).
	//   This example is to show how to use pick.Skip itself; it is roughly a
	//   copy of pick.Seq2's implementation.
	options := map[string]int{"bocchi": 1, "ryo": 2, "nijika": 3, "kita": 4, "kikuri": 5, "seika": 6}
	for range 3 {
		var (
			key  string
			val  int
			skip pick.Skip // Important to use a new Skip for each selection.
			n    uint64
		)
		// Note that we always advance the iteration regardless of whether
		// we accept the yielded term.
		for k, v := range options {
			// First, check whether we are currently skipping.
			// Doing this first means we always accept the first iteration.
			if n > 0 {
				n--
				continue
			}
			// Accept the current term.
			key, val = k, v
			// Get the number of elements to skip before accepting again.
			n = skip.N(rand.Uint64(), rand.Uint64())
		}
		fmt.Println(key, val)
	}
}
